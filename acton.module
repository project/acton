<?php


/**
* Implements hook_menu().
*/
function acton_menu() {
  $items['admin/config/services/act-on'] = array(
  'title' => 'Act-On configuration',
  'description' => 'Configure Act-On and the forms on which Act-On will be used.',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('acton_admin_form'),
  'access arguments' => array('administer acton'),
  'file' => 'acton.admin.inc',
  );

  return $items;
}


/**
 * Implements hook_permission().
 */
function acton_permission() {
  return array(
    'administer acton' => array(
      'title' => t('Administer Act-On'),
      'description' => t('Administer Act-On forms and settings'),
    ),
  );
}


/**
 * Implements hook_form_alter().
 *
 * Add Act-On submit-hook to Act-On enabled forms.
 */
function acton_form_alter(&$form, &$form_state, $form_id) {

  // Don't use for maintenance mode forms (install, update, etc.).
  if (defined('MAINTENANCE_MODE')) {
    return;
  }

  $uncollected_forms = array(
    'user_login',
    'user_login_block',
    'search_form',
    'search_block_form',
    'views_exposed_form',
    'acton_admin_form',
  );

  // If configured to capture all forms, add collection to every form.
  if (variable_get('acton_capture_all_forms', 0) && !in_array($form_id, $uncollected_forms)) {
    // Don't capture system forms - only admins should have access, and system
    // forms may be programmatically submitted by drush and other modules.
    if (strpos($form_id, 'system_') === FALSE && strpos($form_id, 'search_') === FALSE && strpos($form_id, 'views_exposed_form_') === FALSE) {
      acton_add_form_capture($form, $form_state);
    }
  }

  // Otherwise add form capture to admin-configured forms.
  elseif ($forms_to_capture = acton_get_captured_forms()) {
    foreach ($forms_to_capture as $capture_form_id) {
      // For most forms, do a straight check on the form ID.
      if ($form_id == $capture_form_id) {
        acton_add_form_capture($form, $form_state);
      }
      // For webforms use a special check for variable form ID.
      elseif ($capture_form_id == 'webforms' && (strpos($form_id, 'webform_client_form') !== FALSE)) {
        acton_add_form_capture($form, $form_state);
      }
    }
  }
}

/**
 * Build an array of all the collected forms on the site, by form_id.
 *
 */
function acton_get_captured_forms() {
  $forms = &drupal_static(__FUNCTION__);

  // If the data isn't already in memory, get from cache or look it up fresh.
  if (!isset($forms)) {
    if ($cache = cache_get('acton_captured_forms')) {
      $forms = $cache->data;
    }
    else {
      // Look up all the honeypot forms in the variables table.
      $result = db_query("SELECT name FROM {variable} WHERE name LIKE 'acton_form_%'")->fetchCol();
      // Add each form that's enabled to the $forms array.
      foreach ($result as $variable) {
        if (variable_get($variable, 0)) {
          $forms[] = substr($variable, 11);
        }
      }

      // Save the cached data.
      cache_set('acton_captured_forms', $forms, 'cache');
    }
  }
  return $forms;
}

/**
 * Form builder function to add  collection to forms.
 *
 * @return array
 *   Returns submit handler to be placed in a form's elements array.
 */
function acton_add_form_capture(&$form, &$form_state) {
  global $user;

  $form['#submit'][] = 'acton_captured_forms_submit';
  // Allow other modules to alter the collections applied to this form.
  drupal_alter('acton_form_capture', $options, $form);
}



/**
 * Submit fields to acton.
 */
function acton_captured_forms_submit($form, &$form_state) {
  $acton_submitter = new ActonConnection;
  $flat_form = flatten_form_values($form_state['values']);

  foreach ($flat_form as $key => $value) {
    $acton_submitter->setPostItems($key, $value);
  }

  if (variable_get("acton_debug", 0)){
    $message = "Submitting to " . variable_get("acton_url", 0) . ": ";
    foreach ($acton_submitter->getPostItems() as $key => $value) {
      $message .= "<br />\"" . $key . "\" => " . $value . " ";
    }
    drupal_set_message($message,"status");
  }
  if (variable_get("acton_url", 0)) {
    $acton_submitter->processConnection(variable_get("acton_url", 0));
  } else {
    drupal_set_message(l("Act-On url has not been set","admin/config/services/act-on"),"error");
  }
}

function flatten_form_values($ar) {
  $ritit = new RecursiveIteratorIterator(new RecursiveArrayIterator($ar));
  $result = array();
  foreach ($ritit as $leafValue) {
    $keys = array();
    foreach (range(0, $ritit->getDepth()) as $depth) {
      $keys[] = $ritit->getSubIterator($depth)->key();
    }
    $result[ join('_', $keys) ] = $leafValue;
  }
  return $result;
}